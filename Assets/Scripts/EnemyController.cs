using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    [SerializeField] GameObject player;
    private NavMeshAgent agent;
   // private Ray ray;
   // private Ray reflectedRay;
    private RaycastHit hit;
    private RaycastHit secondHit;
    private Collider playerCollider;
    private bool isShoot;
    private Vector3 lookDirection;
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject probe;
    [SerializeField] int rays;
    private int layerMask;
    [SerializeField] float reloadSpeed;
    private bool isFollow;
    
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
        playerCollider = player.GetComponentInChildren<Collider>();
        isShoot = false;
        layerMask = LayerMask.GetMask("Obstacle");
        isFollow = true;
    }

    
    void Update()
    {
        if (isFollow)
        {
            agent.destination = player.transform.position;
        }
        Aim();
        ReflectedAim();
    }

    private void Aim()
    {
        lookDirection = player.transform.position - transform.position;
        if (Physics.Raycast(transform.position, lookDirection, out hit, 20f) && !isShoot)
        {
            if(hit.collider == playerCollider)
            {
                transform.LookAt(player.transform);
                isShoot = true;
                StartCoroutine (Shoot());
                isFollow = false;
                StartCoroutine(Avoid());
                
            }
        }
    }

    private void ReflectedAim()
    {
        
        for(int i = 0; i<rays; i++)
        {
            probe.transform.position = transform.position;
            Vector3 direction = probe.transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position, direction, out hit, 20f, layerMask) && !isShoot)
            {
                Vector3 reflectedDirection = Vector3.Reflect(direction, hit.normal);
                Debug.DrawRay(transform.position, direction * hit.distance, Color.red, 1f);
                Debug.DrawRay(hit.point, hit.normal, Color.red, 1f);

                if(Physics.Raycast(hit.point, reflectedDirection, out secondHit, 20f))
                {
                    if(secondHit.collider == playerCollider)
                    {
                        transform.LookAt(hit.point);
                        isShoot = true;
                        StartCoroutine(Shoot());
                    }
                    Debug.DrawRay(hit.point, reflectedDirection * secondHit.distance, Color.red, 1f);
                    Debug.DrawRay(secondHit.point, secondHit.normal, Color.red, 1f);
                }
            }
            probe.transform.Rotate(Vector3.up, 360 / rays);
        }

    }
    private IEnumerator Shoot()
    {
        Vector3 bulletPosition;
        bulletPosition = transform.position + transform.TransformDirection(Vector3.forward) / 2;
        Instantiate(bullet, bulletPosition, transform.rotation);
        yield return new WaitForSeconds(reloadSpeed);
        isShoot = false;
    }
    private IEnumerator Avoid()
    {
        agent.destination = transform.position + transform.TransformDirection(Vector3.right) * Random.Range(-1f, 1f);
        yield return new WaitForSeconds(0.3f);
        isFollow = true;
    }
}
