using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private int layerMask;
    [SerializeField] float speed =5.0f;
    private bool isShoot;
    [SerializeField] GameObject bullet;
    private Vector3 bulletPosition;
    [SerializeField] float reloadSpeed;
    private RaycastHit hit;
    private int shotsNumber;
    private int reflectedShotsNumber;
    private bool isPlayerLog;
    private float logTime = 5f;
    private List<string> logConstructor;
    [SerializeField] Text logText;
    

    public Vector3 lookDirection { get; private set; }


    void Start()
    {
        layerMask = LayerMask.GetMask("Ground");
        isShoot = false;
        isPlayerLog = true;
        shotsNumber = 0;
        reflectedShotsNumber = 0;
        logConstructor = new List<string>();
        logText.text = "";

    }

    
    void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 20, layerMask))
        {
            Vector3 cursorPosition = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            lookDirection = cursorPosition - transform.position;
            transform.rotation = Quaternion.LookRotation(new Vector3(lookDirection.x, 0, lookDirection.z));
            
        }
        Vector3 dir = Vector3.zero;        
        if (Input.GetKey(KeyCode.A))
            dir.z += -1.0f;
        if (Input.GetKey(KeyCode.D))
            dir.z += 1.0f;
        if (Input.GetKey(KeyCode.W))
            dir.x += -1.0f;
        if (Input.GetKey(KeyCode.S))
            dir.x += 1.0f;

        transform.position += dir.normalized * Time.fixedDeltaTime * speed;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isShoot)
        {
            isShoot = true;
            StartCoroutine(Shoot());
        }

        if (isPlayerLog)
        {
            isPlayerLog = false;
            StartCoroutine(ActionLog());
        }

    }

    private IEnumerator Shoot()
    {
        if (Physics.Raycast(transform.position, lookDirection, out hit, 20f))
        {
            if (hit.collider.CompareTag("Obstacle"))
            {
                reflectedShotsNumber++;

            }
            else
            {
                shotsNumber++;
            }
        }

        bulletPosition = transform.position + transform.TransformDirection(Vector3.forward)* 0.7f;
        Instantiate(bullet, bulletPosition, transform.rotation);
        yield return new WaitForSeconds(reloadSpeed);
        isShoot = false;
    }

    private IEnumerator ActionLog()
    {
        shotsNumber = 0;
        reflectedShotsNumber = 0;
        Vector3 oldPosition = transform.position;
        Vector3 oldRotation = transform.TransformDirection(Vector3.forward);
        yield return new WaitForSeconds(logTime);
        logText.text = "";
        Vector3 newPosition = transform.position;
        Vector3 newRotation = transform.TransformDirection(Vector3.forward);
        Vector3 positionDifference = newPosition - oldPosition;
        float rotationDifference = Vector3.Angle(newRotation, oldRotation);

        if (positionDifference.x > 0.01f)
        {
            logConstructor.Add("����������� ����");
        }
        else if(positionDifference.x < -0.01f)
        {
            logConstructor.Add("����������� �����");
        }
        if(positionDifference.x > 0.01f || positionDifference.x < -0.01f)
        {
            if(positionDifference.z > 0.01)
            {
                logConstructor.Add(" � �������");
            }
            else if(positionDifference.z < -0.01)
            {
                logConstructor.Add(" � ������");
            }
        }
        else
        {
            if (positionDifference.z > 0.01)
            {
                logConstructor.Add("����������� �������");
            }
            else if (positionDifference.z < -0.01)
            {
                logConstructor.Add("����������� ������");
            }
        }
        
        if (logConstructor.Count > 0)
        {
            if (rotationDifference > 0 && rotationDifference <= 90)
            {
                logConstructor.Add(", �������");
            }
            else if(rotationDifference > 90)
            {
                logConstructor.Add(", ��������");
            }
        }
        else
        {
            if (rotationDifference > 0 && rotationDifference <= 90)
            {
                logConstructor.Add("�������");
            }
            else if (rotationDifference > 90)
            {
                logConstructor.Add("��������");
            }
        }

        if(shotsNumber > 0)
        {
            if(logConstructor.Count > 0)
            {
                if(shotsNumber == 1)
                {
                    logConstructor.Add($", {shotsNumber} �������");
                }
                else if(shotsNumber > 1 && shotsNumber < 5)
                {
                    logConstructor.Add($", {shotsNumber} ��������");
                }
                else
                {
                    logConstructor.Add($", {shotsNumber} ���������");
                }
            }
            else
            {
                if (shotsNumber == 1)
                {
                    logConstructor.Add($"{shotsNumber} �������");
                }
                else if (shotsNumber > 1 && shotsNumber < 5)
                {
                    logConstructor.Add($"{shotsNumber} ��������");
                }
                else
                {
                    logConstructor.Add($"{shotsNumber} ���������");
                }
            }
        }
        if (reflectedShotsNumber > 0)
        {
            if (logConstructor.Count > 0)
            {
                if (reflectedShotsNumber == 1)
                {
                    logConstructor.Add($", {reflectedShotsNumber} ������� c �������� �������");
                }
                else if (reflectedShotsNumber > 1 && reflectedShotsNumber < 5)
                {
                    logConstructor.Add($", {reflectedShotsNumber} �������� c �������� �������");
                }
                else
                {
                    logConstructor.Add($", {reflectedShotsNumber} ��������� c �������� �������");
                }
            }
            else
            {
                if (reflectedShotsNumber == 1)
                {
                    logConstructor.Add($"{reflectedShotsNumber} ������� c �������� �������");
                }
                else if (reflectedShotsNumber > 1 && reflectedShotsNumber < 5)
                {
                    logConstructor.Add($"{reflectedShotsNumber} �������� c �������� �������");
                }
                else
                {
                    logConstructor.Add($"{reflectedShotsNumber} ��������� c �������� �������");
                }
            }
        }

        if(logConstructor.Count > 0)
        {
            logConstructor.Add(".");
        }

        for(int i = 0; i < logConstructor.Count; i++)
        {
            logText.text += logConstructor[i];
        }

        logConstructor.Clear();

        isPlayerLog = true;
    }

}
