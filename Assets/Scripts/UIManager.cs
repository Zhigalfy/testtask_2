using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject title;
    [SerializeField] Button startButton;
    [SerializeField] Button ExitButton;
    [SerializeField] Text titleText;
    [SerializeField] Text scoreText;
    [SerializeField] BulletController bulletController;
    [SerializeField] GameObject cursor;
    private GameObject player;
    private GameManager gameManager;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        scoreText.text = $"SCORE: {gameManager.playerScore} : {gameManager.enemyScore}";
        startButton.onClick.AddListener(StartGame);
        ExitButton.onClick.AddListener(ExitGame);
    }

    void Start()
    {
        if (gameManager.isFirstRound)
        {
           
            title.transform.position = new Vector2(Screen.width / 2, Screen.height / 2);
            player.gameObject.SetActive(false);
            titleText.gameObject.SetActive(true);
            titleText.text = "SHOOTER GAME";
            startButton.gameObject.SetActive(true);
            ExitButton.gameObject.SetActive(true);
           
            // scoreText.gameObject.SetActive(true);
            cursor.gameObject.SetActive(false);
            
            Time.timeScale = 0f;
        }
        else
        {
            StartGame();
        }
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }

    }

    public void StartGame()
    {
        title.gameObject.SetActive(false);
        player.gameObject.SetActive(true);
       // scoreText.gameObject.SetActive(true);
        cursor.gameObject.SetActive(true);
        Time.timeScale = 1f;
    }

    public void PauseGame()
    {
        title.gameObject.SetActive(true);
        titleText.text = "PAUSE";
        startButton.gameObject.SetActive(true);
        ExitButton.gameObject.SetActive(true);
       // scoreText.gameObject.SetActive(true);
        cursor.gameObject.SetActive(false);
        player.gameObject.SetActive(false);
        Time.timeScale = 0f;
    }

    public void ExitGame()
    {
#if UNITY_EDITOR

        UnityEditor.EditorApplication.ExitPlaymode();

#endif
        Application.Quit();
    }

    public void GameOver()
    {
        gameManager.isFirstRound = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
       
    }

    
    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            PauseGame();
        }
    }

}
