using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] float bulletSpeed;
    [SerializeField] Vector3 shootDirection;
    private Vector3 reflectedDirection;
    Ray ray;
    RaycastHit hit;
    private int layerMask;
    private UIManager manager;
    private GameManager gameManager;
   

    void Start()
    {
        shootDirection = transform.TransformDirection(Vector3.forward);
        //shootDirection = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().lookDirection;
        layerMask = LayerMask.GetMask("Obstacle");
        manager = GameObject.Find("UIManager").GetComponent<UIManager>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        Reflection();
    }

    
    void FixedUpdate()
    {
        


       // Reflection();

    }

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * bulletSpeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle") && hit.point != Vector3.zero)
        {
            shootDirection = reflectedDirection.normalized;
            transform.position = hit.point + shootDirection * 0.01f;
            transform.rotation = Quaternion.LookRotation(shootDirection);
            Reflection();
        }
        if (collision.gameObject.CompareTag("Bound"))
        {
            
            Destroy(this.gameObject);

        }
        if (collision.gameObject.CompareTag("Player"))
        {
            gameManager.enemyScore++;
            manager.GameOver();
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            gameManager.playerScore++;
            manager.GameOver();
        }
    }

    
    private void Reflection()
    {
        ray = new Ray (transform.position, shootDirection);

        if (Physics.Raycast(ray, out hit, 20f, layerMask))
        {
            reflectedDirection = Vector3.Reflect(shootDirection, hit.normal);
            Debug.DrawRay(transform.position, shootDirection * hit.distance, Color.red, 1f);
            Debug.DrawRay(hit.point, hit.normal, Color.red, 1f);
            
        }
    }
}
